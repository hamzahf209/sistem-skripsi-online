<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Koreksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}


	public function index()
	{
        // $data['koreksidata'] = array('text' =>"");
        
	}

	public function proses()
	{
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_PORT => "3000",
          CURLOPT_URL => "http://localhost:3000/api/utils/readDocx",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 300,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: c174f711-3ffd-f4ad-5655-31de52b39e27"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          $data['koreksidata']=$response;
          $response;
          json_encode(array('koreksidata'=>$response));
        //  $this->load->view('mahasiswa/mySkripsi', $data);
        }
	}
}
/* End of file controllername.php */
/* Location: ./application/controllers/controllername.php */