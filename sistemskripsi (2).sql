-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Agu 2021 pada 05.03
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistemskripsi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `Password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `Password`) VALUES
(0, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ideskripsi`
--

CREATE TABLE `ideskripsi` (
  `IDIde` bigint(20) NOT NULL,
  `IDIdeMahasiswa` bigint(20) NOT NULL,
  `JudulIde` varchar(100) NOT NULL,
  `DeskripsiIde` text NOT NULL,
  `TanggalIde` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `IDJurusan` bigint(20) NOT NULL,
  `Jurusan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`IDJurusan`, `Jurusan`) VALUES
(1, 'Teknik Informatika'),
(2, 'Hukum'),
(3, 'Komunikasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kartubimbingan`
--

CREATE TABLE `kartubimbingan` (
  `IDKartu` int(11) NOT NULL,
  `IDKartuMahasiswa` bigint(30) NOT NULL,
  `IDDosenPembimbing` varchar(30) NOT NULL,
  `Catatan` text NOT NULL,
  `TanggalBimbingan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kartubimbingan`
--

INSERT INTO `kartubimbingan` (`IDKartu`, `IDKartuMahasiswa`, `IDDosenPembimbing`, `Catatan`, `TanggalBimbingan`) VALUES
(3, 1, '102', '[{\"text\":\"terstuktur\",\"line\":8,\"bahasa\":[\"latin\",0.3626666666666667]},{\"text\":\"digital\",\"line\":10,\"bahasa\":[\"indonesian\",0.41000000000000003]},{\"text\":\"inilah\",\"line\":10,\"bahasa\":[\"indonesian\",0.5694444444444444]},{\"text\":\"internet\",\"line\":10,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"Dikutip\",\"line\":18,\"bahasa\":[\"lithuanian\",0.1561904761904762]},{\"text\":\"Wayne\",\"line\":18,\"bahasa\":[\"hausa\",0.35]},{\"text\":\"Atmodiwiro\",\"line\":18,\"bahasa\":[\"danish\",0.09666666666666668]},{\"text\":\"organic\",\"line\":18,\"bahasa\":[\"indonesian\",0.3114285714285714]},{\"text\":\"Daryanto\",\"line\":18,\"bahasa\":[\"italian\",0.38708333333333333]},{\"text\":\"Dikutip\",\"line\":20,\"bahasa\":[\"lithuanian\",0.1561904761904762]},{\"text\":\"Daradjat\",\"line\":20,\"bahasa\":[\"indonesian\",0.4125]},{\"text\":\"efektip\",\"line\":24,\"bahasa\":[\"swedish\",0.281904761904762]},{\"text\":\"terintegrasi\",\"line\":32,\"bahasa\":[\"hungarian\",0.35305555555555557]},{\"text\":\"Ditengah\",\"line\":32,\"bahasa\":[\"indonesian\",0.48458333333333337]},{\"text\":\"Google\",\"line\":36,\"bahasa\":[\"pidgin\",0.3127777777777778]},{\"text\":\"Classroom\",\"line\":36,\"bahasa\":[\"slovak\",0.17925925925925923]},{\"text\":\"Zoom\",\"line\":36,\"bahasa\":[\"swedish\",0.2466666666666667]},{\"text\":\"Google\",\"line\":36,\"bahasa\":[\"pidgin\",0.3127777777777778]},{\"text\":\"Google\",\"line\":36,\"bahasa\":[\"pidgin\",0.3127777777777778]},{\"text\":\"Google\",\"line\":36,\"bahasa\":[\"pidgin\",0.3127777777777778]},{\"text\":\"Drive\",\"line\":36,\"bahasa\":[\"slovene\",0.29066666666666663]},{\"text\":\"WhatsApp\",\"line\":36,\"bahasa\":[\"english\",0.22458333333333336]},{\"text\":\"online\",\"line\":38,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"fitur\",\"line\":38,\"bahasa\":[\"latin\",0.7206666666666667]},{\"text\":\"learning\",\"line\":40,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"electronica\",\"line\":40,\"bahasa\":[\"croatian\",0.2245454545454546]},{\"text\":\"learning\",\"line\":40,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":40,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"Dikutip\",\"line\":40,\"bahasa\":[\"lithuanian\",0.1561904761904762]},{\"text\":\"Onno\",\"line\":40,\"bahasa\":[\"italian\",0.34750000000000003]},{\"text\":\"electronica\",\"line\":40,\"bahasa\":[\"croatian\",0.2245454545454546]},{\"text\":\"learninging\",\"line\":40,\"bahasa\":[\"norwegian\",0.30966666666666665]},{\"text\":\"internet\",\"line\":40,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":42,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":42,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"learning\",\"line\":42,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":42,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"learning\",\"line\":42,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"internet\",\"line\":42,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":44,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":44,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"internet\",\"line\":44,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":46,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":46,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":46,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"fitur\",\"line\":46,\"bahasa\":[\"latin\",0.7206666666666667]},{\"text\":\"fitur\",\"line\":46,\"bahasa\":[\"latin\",0.7206666666666667]},{\"text\":\"online\",\"line\":48,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"terintegritas\",\"line\":48,\"bahasa\":[\"latin\",0.3835897435897435]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"online\",\"line\":48,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"online\",\"line\":48,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"internet\",\"line\":48,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"intranet\",\"line\":48,\"bahasa\":[\"swedish\",0.355]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"WLAN\",\"line\":48,\"bahasa\":[\"cebuano\",0.4658333333333333]},{\"text\":\"internet\",\"line\":48,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"online\",\"line\":50,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"learning\",\"line\":50,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":56,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":58,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":58,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"efesiensi\",\"line\":58,\"bahasa\":[\"turkish\",0.18259259259259253]},{\"text\":\"learning\",\"line\":64,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"online\",\"line\":64,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"quiz\",\"line\":64,\"bahasa\":[\"latin\",0.45999999999999996]},{\"text\":\"terintegrasi\",\"line\":64,\"bahasa\":[\"hungarian\",0.35305555555555557]},{\"text\":\"learning\",\"line\":64,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"online\",\"line\":64,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"learning\",\"line\":68,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":68,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"efesiensi\",\"line\":68,\"bahasa\":[\"turkish\",0.18259259259259253]},{\"text\":\"learning\",\"line\":74,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":76,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"login\",\"line\":78,\"bahasa\":[\"icelandic\",0.31199999999999994]},{\"text\":\"learning\",\"line\":82,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"literatur\",\"line\":82,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"Development\",\"line\":82,\"bahasa\":[\"french\",0.353030303030303]},{\"text\":\"Waterfall\",\"line\":82,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"Denzig\",\"line\":88,\"bahasa\":[\"danish\",0.4455555555555555]},{\"text\":\"tersetruktur\",\"line\":88,\"bahasa\":[\"latin\",0.30333333333333334]},{\"text\":\"Literatur\",\"line\":94,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"literatur\",\"line\":96,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"refrensi\",\"line\":96,\"bahasa\":[\"spanish\",0.21250000000000002]},{\"text\":\"learning\",\"line\":96,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"Development\",\"line\":100,\"bahasa\":[\"french\",0.353030303030303]},{\"text\":\"Waterfall\",\"line\":100,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"waterfall\",\"line\":100,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"Waterfall\",\"line\":104,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"Requiremenst\",\"line\":106,\"bahasa\":[\"french\",0.39861111111111114]},{\"text\":\"Analysis\",\"line\":106,\"bahasa\":[\"tagalog\",0.3154166666666667]},{\"text\":\"literatur\",\"line\":108,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"relavan\",\"line\":108,\"bahasa\":[\"dutch\",0.3271428571428572]},{\"text\":\"Design\",\"line\":110,\"bahasa\":[\"french\",0.3088888888888889]},{\"text\":\"Development\",\"line\":114,\"bahasa\":[\"french\",0.353030303030303]},{\"text\":\"Dibagian\",\"line\":116,\"bahasa\":[\"indonesian\",0.44666666666666666]},{\"text\":\"learning\",\"line\":120,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"tlah\",\"line\":120,\"bahasa\":[\"indonesian\",0.43666666666666665]},{\"text\":\"error\",\"line\":120,\"bahasa\":[\"norwegian\",0.374]},{\"text\":\"IPENDAHULUAN\",\"line\":130,\"bahasa\":[\"indonesian\",0.2758333333333334]},{\"text\":\"relavan\",\"line\":132,\"bahasa\":[\"dutch\",0.3271428571428572]},{\"text\":\"refrensinya\",\"line\":132,\"bahasa\":[\"indonesian\",0.2284848484848484]},{\"text\":\"IIIANALISIS\",\"line\":134,\"bahasa\":[\"tagalog\",0.22636363636363632]},{\"text\":\"case\",\"line\":136,\"bahasa\":[\"spanish\",0.47583333333333333]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"activity\",\"line\":136,\"bahasa\":[\"spanish\",0.2104166666666667]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"class\",\"line\":136,\"bahasa\":[\"estonian\",0.15466666666666673]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"entity\",\"line\":136,\"bahasa\":[\"latin\",0.3844444444444445]},{\"text\":\"relationship\",\"line\":136,\"bahasa\":[\"french\",0.38416666666666666]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"output\",\"line\":136,\"bahasa\":[\"english\",0.2305555555555555]},{\"text\":\"input\",\"line\":136,\"bahasa\":[\"latin\",0.3946666666666667]},{\"text\":\"interface\",\"line\":136,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"VIMPLEMENTASI\",\"line\":138,\"bahasa\":[\"lithuanian\",0.3389743589743589]},{\"text\":\"output\",\"line\":138,\"bahasa\":[\"english\",0.2305555555555555]},{\"text\":\"input\",\"line\":138,\"bahasa\":[\"latin\",0.3946666666666667]},{\"text\":\"VIPENUTUP\",\"line\":140,\"bahasa\":[\"finnish\",0.14222222222222225]},{\"text\":\"learning\",\"line\":152,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"literatur\",\"line\":162,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"refrensi\",\"line\":162,\"bahasa\":[\"spanish\",0.21250000000000002]},{\"text\":\"learning\",\"line\":162,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"terhadapat\",\"line\":166,\"bahasa\":[\"indonesian\",0.40966666666666673]},{\"text\":\"literatur\",\"line\":166,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"relavan\",\"line\":166,\"bahasa\":[\"dutch\",0.3271428571428572]},{\"text\":\"case\",\"line\":170,\"bahasa\":[\"spanish\",0.47583333333333333]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"activity\",\"line\":170,\"bahasa\":[\"spanish\",0.2104166666666667]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"entity\",\"line\":170,\"bahasa\":[\"latin\",0.3844444444444445]},{\"text\":\"relationship\",\"line\":170,\"bahasa\":[\"french\",0.38416666666666666]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"class\",\"line\":170,\"bahasa\":[\"estonian\",0.15466666666666673]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"tools\",\"line\":170,\"bahasa\":[\"english\",0.19733333333333325]},{\"text\":\"Draw\",\"line\":170,\"bahasa\":[\"slovene\",0.19833333333333336]},{\"text\":\"Interface\",\"line\":172,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"interdace\",\"line\":174,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"experience\",\"line\":174,\"bahasa\":[\"spanish\",0.2686666666666666]},{\"text\":\"tools\",\"line\":174,\"bahasa\":[\"english\",0.19733333333333325]},{\"text\":\"Adobe\",\"line\":174,\"bahasa\":[\"portuguese\",0.20266666666666666]},{\"text\":\"interface\",\"line\":174,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"interface\",\"line\":174,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"learning\",\"line\":174,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"framework\",\"line\":178,\"bahasa\":[\"pidgin\",0.18555555555555547]},{\"text\":\"laravel\",\"line\":178,\"bahasa\":[\"spanish\",0.42714285714285716]},{\"text\":\"Chrome\",\"line\":178,\"bahasa\":[\"italian\",0.2644444444444445]},{\"text\":\"local\",\"line\":178,\"bahasa\":[\"spanish\",0.3533333333333334]},{\"text\":\"XAMPP\",\"line\":178,\"bahasa\":[\"somali\",0.10733333333333328]},{\"text\":\"learning\",\"line\":182,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektiftas\",\"line\":182,\"bahasa\":[\"swedish\",0.278]},{\"text\":\"efesiensi\",\"line\":182,\"bahasa\":[\"turkish\",0.18259259259259253]},{\"text\":\"online\",\"line\":182,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"Chrome\",\"line\":182,\"bahasa\":[\"italian\",0.2644444444444445]},{\"text\":\"Learning\",\"line\":184,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":186,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efesien\",\"line\":186,\"bahasa\":[\"spanish\",0.28047619047619043]}]', '2021-07-15'),
(4, 1, '102', ' contoh nya', '2021-07-15'),
(5, 1, '102', '[{\"text\":\"sspasi\",\"line\":4,\"bahasa\":[\"lithuanian\",0.34444444444444444]},{\"text\":\"terstuktur\",\"line\":8,\"bahasa\":[\"latin\",0.3626666666666667]},{\"text\":\"passal\",\"line\":8,\"bahasa\":[\"portuguese\",0.3683333333333333]},{\"text\":\"digital\",\"line\":10,\"bahasa\":[\"indonesian\",0.41000000000000003]},{\"text\":\"inilah\",\"line\":10,\"bahasa\":[\"indonesian\",0.5694444444444444]},{\"text\":\"internet\",\"line\":10,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"seriring\",\"line\":12,\"bahasa\":[\"norwegian\",0.5583333333333333]},{\"text\":\"Dikutip\",\"line\":18,\"bahasa\":[\"lithuanian\",0.1561904761904762]},{\"text\":\"Wayne\",\"line\":18,\"bahasa\":[\"hausa\",0.35]},{\"text\":\"Soebagio\",\"line\":18,\"bahasa\":[\"italian\",0.23124999999999996]},{\"text\":\"Atmodiwiro\",\"line\":18,\"bahasa\":[\"danish\",0.09666666666666668]},{\"text\":\"Daryanto\",\"line\":18,\"bahasa\":[\"italian\",0.38708333333333333]},{\"text\":\"Dikutip\",\"line\":20,\"bahasa\":[\"lithuanian\",0.1561904761904762]},{\"text\":\"Sadirman\",\"line\":20,\"bahasa\":[\"azeri\",0.39125]},{\"text\":\"kesekolah\",\"line\":20,\"bahasa\":[\"indonesian\",0.3037037037037037]},{\"text\":\"Daradjat\",\"line\":20,\"bahasa\":[\"indonesian\",0.4125]},{\"text\":\"kondusip\",\"line\":24,\"bahasa\":[\"lithuanian\",0.31625000000000003]},{\"text\":\"efektip\",\"line\":24,\"bahasa\":[\"swedish\",0.281904761904762]},{\"text\":\"sangatlah\",\"line\":26,\"bahasa\":[\"indonesian\",0.6448148148148147]},{\"text\":\"kependidikan\",\"line\":28,\"bahasa\":[\"indonesian\",0.39249999999999996]},{\"text\":\"terintegrasi\",\"line\":32,\"bahasa\":[\"hungarian\",0.35305555555555557]},{\"text\":\"Ditengah\",\"line\":32,\"bahasa\":[\"indonesian\",0.48458333333333337]},{\"text\":\"Sudjana\",\"line\":34,\"bahasa\":[\"hausa\",0.39]},{\"text\":\"Google\",\"line\":36,\"bahasa\":[\"pidgin\",0.3127777777777778]},{\"text\":\"Classroom\",\"line\":36,\"bahasa\":[\"slovak\",0.17925925925925923]},{\"text\":\"Zoom\",\"line\":36,\"bahasa\":[\"swedish\",0.2466666666666667]},{\"text\":\"Google\",\"line\":36,\"bahasa\":[\"pidgin\",0.3127777777777778]},{\"text\":\"Google\",\"line\":36,\"bahasa\":[\"pidgin\",0.3127777777777778]},{\"text\":\"Google\",\"line\":36,\"bahasa\":[\"pidgin\",0.3127777777777778]},{\"text\":\"Drive\",\"line\":36,\"bahasa\":[\"slovene\",0.29066666666666663]},{\"text\":\"WhatsApp\",\"line\":36,\"bahasa\":[\"english\",0.22458333333333336]},{\"text\":\"fitur\",\"line\":38,\"bahasa\":[\"latin\",0.7206666666666667]},{\"text\":\"learning\",\"line\":40,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"electronica\",\"line\":40,\"bahasa\":[\"croatian\",0.2245454545454546]},{\"text\":\"learning\",\"line\":40,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":40,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"Dikutip\",\"line\":40,\"bahasa\":[\"lithuanian\",0.1561904761904762]},{\"text\":\"Purbo\",\"line\":40,\"bahasa\":[\"cebuano\",0.1186666666666667]},{\"text\":\"electronica\",\"line\":40,\"bahasa\":[\"croatian\",0.2245454545454546]},{\"text\":\"learninging\",\"line\":40,\"bahasa\":[\"norwegian\",0.30966666666666665]},{\"text\":\"internet\",\"line\":40,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":42,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":42,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"Pujiriyanto\",\"line\":42,\"bahasa\":[\"tagalog\",0.31272727272727274]},{\"text\":\"learning\",\"line\":42,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":42,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"learning\",\"line\":42,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"internet\",\"line\":42,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":44,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":44,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"internet\",\"line\":44,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":46,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":46,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"source\",\"line\":46,\"bahasa\":[\"czech\",0.395]},{\"text\":\"perlukah\",\"line\":46,\"bahasa\":[\"indonesian\",0.44333333333333336]},{\"text\":\"staff\",\"line\":46,\"bahasa\":[\"czech\",0.378]},{\"text\":\"learning\",\"line\":46,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"fitur\",\"line\":46,\"bahasa\":[\"latin\",0.7206666666666667]},{\"text\":\"fitur\",\"line\":46,\"bahasa\":[\"latin\",0.7206666666666667]},{\"text\":\"Berdasakrkan\",\"line\":48,\"bahasa\":[\"indonesian\",0.39888888888888885]},{\"text\":\"terintegritas\",\"line\":48,\"bahasa\":[\"latin\",0.3835897435897435]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"Hartley\",\"line\":48,\"bahasa\":[\"danish\",0.271904761904762]},{\"text\":\"Hartley\",\"line\":48,\"bahasa\":[\"danish\",0.271904761904762]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"internet\",\"line\":48,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"intranet\",\"line\":48,\"bahasa\":[\"swedish\",0.355]},{\"text\":\"Kumar\",\"line\":48,\"bahasa\":[\"hausa\",0.722]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"WLAN\",\"line\":48,\"bahasa\":[\"cebuano\",0.4658333333333333]},{\"text\":\"internet\",\"line\":48,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":50,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":56,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"staff\",\"line\":56,\"bahasa\":[\"czech\",0.378]},{\"text\":\"pengimplementasian\",\"line\":58,\"bahasa\":[\"indonesian\",0.4227777777777778]},{\"text\":\"learning\",\"line\":58,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"staff\",\"line\":58,\"bahasa\":[\"czech\",0.378]},{\"text\":\"efektifitas\",\"line\":58,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"efesiensi\",\"line\":58,\"bahasa\":[\"turkish\",0.18259259259259253]},{\"text\":\"learning\",\"line\":64,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"quiz\",\"line\":64,\"bahasa\":[\"latin\",0.45999999999999996]},{\"text\":\"terintegrasi\",\"line\":64,\"bahasa\":[\"hungarian\",0.35305555555555557]},{\"text\":\"learning\",\"line\":64,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":68,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":68,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"efesiensi\",\"line\":68,\"bahasa\":[\"turkish\",0.18259259259259253]},{\"text\":\"learning\",\"line\":74,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":76,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"login\",\"line\":78,\"bahasa\":[\"icelandic\",0.31199999999999994]},{\"text\":\"learning\",\"line\":82,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"literatur\",\"line\":82,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"System\",\"line\":82,\"bahasa\":[\"portuguese\",0.3961111111111112]},{\"text\":\"Development\",\"line\":82,\"bahasa\":[\"french\",0.353030303030303]},{\"text\":\"Waterfall\",\"line\":82,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"penelitu\",\"line\":82,\"bahasa\":[\"indonesian\",0.34875]},{\"text\":\"Denzig\",\"line\":88,\"bahasa\":[\"danish\",0.4455555555555555]},{\"text\":\"tersetruktur\",\"line\":88,\"bahasa\":[\"latin\",0.30333333333333334]},{\"text\":\"Staff\",\"line\":88,\"bahasa\":[\"czech\",0.378]},{\"text\":\"staff\",\"line\":92,\"bahasa\":[\"czech\",0.378]},{\"text\":\"Literatur\",\"line\":94,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"literatur\",\"line\":96,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"refrensi\",\"line\":96,\"bahasa\":[\"spanish\",0.21250000000000002]},{\"text\":\"learning\",\"line\":96,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"System\",\"line\":100,\"bahasa\":[\"portuguese\",0.3961111111111112]},{\"text\":\"Development\",\"line\":100,\"bahasa\":[\"french\",0.353030303030303]},{\"text\":\"Waterfall\",\"line\":100,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"SDLC\",\"line\":100},{\"text\":\"SDLC\",\"line\":100},{\"text\":\"waterfall\",\"line\":100,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"SDLC\",\"line\":104},{\"text\":\"Waterfall\",\"line\":104,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"Requiremenst\",\"line\":106,\"bahasa\":[\"french\",0.39861111111111114]},{\"text\":\"Analysis\",\"line\":106,\"bahasa\":[\"tagalog\",0.3154166666666667]},{\"text\":\"kusioner\",\"line\":108,\"bahasa\":[\"italian\",0.4108333333333334]},{\"text\":\"literatur\",\"line\":108,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"relavan\",\"line\":108,\"bahasa\":[\"dutch\",0.3271428571428572]},{\"text\":\"Design\",\"line\":110,\"bahasa\":[\"french\",0.3088888888888889]},{\"text\":\"pemodelan\",\"line\":112,\"bahasa\":[\"indonesian\",0.38370370370370377]},{\"text\":\"Development\",\"line\":114,\"bahasa\":[\"french\",0.353030303030303]},{\"text\":\"Dibagian\",\"line\":116,\"bahasa\":[\"indonesian\",0.44666666666666666]},{\"text\":\"staff\",\"line\":116,\"bahasa\":[\"czech\",0.378]},{\"text\":\"learning\",\"line\":120,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"tlah\",\"line\":120,\"bahasa\":[\"indonesian\",0.43666666666666665]},{\"text\":\"error\",\"line\":120,\"bahasa\":[\"norwegian\",0.374]},{\"text\":\"bugs\",\"line\":120,\"bahasa\":[\"turkish\",0.22999999999999998]},{\"text\":\"IPENDAHULUAN\",\"line\":130,\"bahasa\":[\"indonesian\",0.2758333333333334]},{\"text\":\"relavan\",\"line\":132,\"bahasa\":[\"dutch\",0.3271428571428572]},{\"text\":\"refrensinya\",\"line\":132,\"bahasa\":[\"indonesian\",0.2284848484848484]},{\"text\":\"IIIANALISIS\",\"line\":134,\"bahasa\":[\"tagalog\",0.22636363636363632]},{\"text\":\"bseserta\",\"line\":134,\"bahasa\":[\"indonesian\",0.20375]},{\"text\":\"pemodelan\",\"line\":136,\"bahasa\":[\"indonesian\",0.38370370370370377]},{\"text\":\"case\",\"line\":136,\"bahasa\":[\"spanish\",0.47583333333333333]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"activity\",\"line\":136,\"bahasa\":[\"spanish\",0.2104166666666667]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"sequence\",\"line\":136,\"bahasa\":[\"french\",0.4475]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"class\",\"line\":136,\"bahasa\":[\"estonian\",0.15466666666666673]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"entity\",\"line\":136,\"bahasa\":[\"latin\",0.3844444444444445]},{\"text\":\"relationship\",\"line\":136,\"bahasa\":[\"french\",0.38416666666666666]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"perangcangan\",\"line\":136,\"bahasa\":[\"indonesian\",0.6724242424242424]},{\"text\":\"input\",\"line\":136,\"bahasa\":[\"latin\",0.3946666666666667]},{\"text\":\"interface\",\"line\":136,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"VIMPLEMENTASI\",\"line\":138,\"bahasa\":[\"lithuanian\",0.3389743589743589]},{\"text\":\"input\",\"line\":138,\"bahasa\":[\"latin\",0.3946666666666667]},{\"text\":\"VIPENUTUP\",\"line\":140,\"bahasa\":[\"finnish\",0.14222222222222225]},{\"text\":\"learning\",\"line\":152,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"literatur\",\"line\":162,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"refrensi\",\"line\":162,\"bahasa\":[\"spanish\",0.21250000000000002]},{\"text\":\"learning\",\"line\":162,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"terhadapat\",\"line\":166,\"bahasa\":[\"indonesian\",0.40966666666666673]},{\"text\":\"literatur\",\"line\":166,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"relavan\",\"line\":166,\"bahasa\":[\"dutch\",0.3271428571428572]},{\"text\":\"staff\",\"line\":166,\"bahasa\":[\"czech\",0.378]},{\"text\":\"case\",\"line\":170,\"bahasa\":[\"spanish\",0.47583333333333333]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"activity\",\"line\":170,\"bahasa\":[\"spanish\",0.2104166666666667]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"entity\",\"line\":170,\"bahasa\":[\"latin\",0.3844444444444445]},{\"text\":\"relationship\",\"line\":170,\"bahasa\":[\"french\",0.38416666666666666]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"sequence\",\"line\":170,\"bahasa\":[\"french\",0.4475]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"class\",\"line\":170,\"bahasa\":[\"estonian\",0.15466666666666673]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"tools\",\"line\":170,\"bahasa\":[\"english\",0.19733333333333325]},{\"text\":\"Draw\",\"line\":170,\"bahasa\":[\"slovene\",0.19833333333333336]},{\"text\":\"Interface\",\"line\":172,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"interdace\",\"line\":174,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"experience\",\"line\":174,\"bahasa\":[\"spanish\",0.2686666666666666]},{\"text\":\"tools\",\"line\":174,\"bahasa\":[\"english\",0.19733333333333325]},{\"text\":\"Adobe\",\"line\":174,\"bahasa\":[\"portuguese\",0.20266666666666666]},{\"text\":\"interface\",\"line\":174,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"interface\",\"line\":174,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"learning\",\"line\":174,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"framework\",\"line\":178,\"bahasa\":[\"pidgin\",0.18555555555555547]},{\"text\":\"laravel\",\"line\":178,\"bahasa\":[\"spanish\",0.42714285714285716]},{\"text\":\"HTML\",\"line\":178},{\"text\":\"Chrome\",\"line\":178,\"bahasa\":[\"italian\",0.2644444444444445]},{\"text\":\"local\",\"line\":178,\"bahasa\":[\"spanish\",0.3533333333333334]},{\"text\":\"XAMPP\",\"line\":178,\"bahasa\":[\"somali\",0.10733333333333328]},{\"text\":\"hosting\",\"line\":178,\"bahasa\":[\"english\",0.5033333333333334]},{\"text\":\"learning\",\"line\":182,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"staff\",\"line\":182,\"bahasa\":[\"czech\",0.378]},{\"text\":\"efektiftas\",\"line\":182,\"bahasa\":[\"swedish\",0.278]},{\"text\":\"efesiensi\",\"line\":182,\"bahasa\":[\"turkish\",0.18259259259259253]},{\"text\":\"Chrome\",\"line\":182,\"bahasa\":[\"italian\",0.2644444444444445]},{\"text\":\"Learning\",\"line\":184,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"perilisan\",\"line\":186,\"bahasa\":[\"indonesian\",0.5455555555555556]},{\"text\":\"learning\",\"line\":186,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"staff\",\"line\":186,\"bahasa\":[\"czech\",0.378]},{\"text\":\"efesien\",\"line\":186,\"bahasa\":[\"spanish\",0.28047619047619043]}]', '2021-07-15'),
(6, 1, '102', '[{\"text\":\"terstuktur\",\"line\":8,\"bahasa\":[\"latin\",0.3626666666666667]},{\"text\":\"inilah\",\"line\":10,\"bahasa\":[\"indonesian\",0.5694444444444444]},{\"text\":\"internet\",\"line\":10,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"Dikutip\",\"line\":18,\"bahasa\":[\"lithuanian\",0.1561904761904762]},{\"text\":\"Wayne\",\"line\":18,\"bahasa\":[\"hausa\",0.35]},{\"text\":\"Atmodiwiro\",\"line\":18,\"bahasa\":[\"danish\",0.09666666666666668]},{\"text\":\"organic\",\"line\":18,\"bahasa\":[\"indonesian\",0.3114285714285714]},{\"text\":\"Daryanto\",\"line\":18,\"bahasa\":[\"italian\",0.38708333333333333]},{\"text\":\"Dikutip\",\"line\":20,\"bahasa\":[\"lithuanian\",0.1561904761904762]},{\"text\":\"Daradjat\",\"line\":20,\"bahasa\":[\"indonesian\",0.4125]},{\"text\":\"efektip\",\"line\":24,\"bahasa\":[\"swedish\",0.281904761904762]},{\"text\":\"terintegrasi\",\"line\":32,\"bahasa\":[\"hungarian\",0.35305555555555557]},{\"text\":\"Ditengah\",\"line\":32,\"bahasa\":[\"indonesian\",0.48458333333333337]},{\"text\":\"Classroom\",\"line\":36,\"bahasa\":[\"slovak\",0.17925925925925923]},{\"text\":\"Zoom\",\"line\":36,\"bahasa\":[\"swedish\",0.2466666666666667]},{\"text\":\"Drive\",\"line\":36,\"bahasa\":[\"slovene\",0.29066666666666663]},{\"text\":\"WhatsApp\",\"line\":36,\"bahasa\":[\"english\",0.22458333333333336]},{\"text\":\"online\",\"line\":38,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"fitur\",\"line\":38,\"bahasa\":[\"latin\",0.7206666666666667]},{\"text\":\"learning\",\"line\":40,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"electronica\",\"line\":40,\"bahasa\":[\"croatian\",0.2245454545454546]},{\"text\":\"learning\",\"line\":40,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":40,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"Dikutip\",\"line\":40,\"bahasa\":[\"lithuanian\",0.1561904761904762]},{\"text\":\"Onno\",\"line\":40,\"bahasa\":[\"italian\",0.34750000000000003]},{\"text\":\"electronica\",\"line\":40,\"bahasa\":[\"croatian\",0.2245454545454546]},{\"text\":\"learninging\",\"line\":40,\"bahasa\":[\"norwegian\",0.30966666666666665]},{\"text\":\"internet\",\"line\":40,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":42,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":42,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"learning\",\"line\":42,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":42,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"learning\",\"line\":42,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"internet\",\"line\":42,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":44,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":44,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"internet\",\"line\":44,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"learning\",\"line\":46,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":46,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":46,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"fitur\",\"line\":46,\"bahasa\":[\"latin\",0.7206666666666667]},{\"text\":\"fitur\",\"line\":46,\"bahasa\":[\"latin\",0.7206666666666667]},{\"text\":\"online\",\"line\":48,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"terintegritas\",\"line\":48,\"bahasa\":[\"latin\",0.3835897435897435]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"online\",\"line\":48,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"Hartley\",\"line\":48,\"bahasa\":[\"danish\",0.271904761904762]},{\"text\":\"Hartley\",\"line\":48,\"bahasa\":[\"danish\",0.271904761904762]},{\"text\":\"online\",\"line\":48,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"internet\",\"line\":48,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"intranet\",\"line\":48,\"bahasa\":[\"swedish\",0.355]},{\"text\":\"learning\",\"line\":48,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"WLAN\",\"line\":48,\"bahasa\":[\"cebuano\",0.4658333333333333]},{\"text\":\"internet\",\"line\":48,\"bahasa\":[\"danish\",0.5908333333333333]},{\"text\":\"online\",\"line\":50,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"learning\",\"line\":50,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":56,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":58,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":58,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"efesiensi\",\"line\":58,\"bahasa\":[\"turkish\",0.18259259259259253]},{\"text\":\"learning\",\"line\":64,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"online\",\"line\":64,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"quiz\",\"line\":64,\"bahasa\":[\"latin\",0.45999999999999996]},{\"text\":\"terintegrasi\",\"line\":64,\"bahasa\":[\"hungarian\",0.35305555555555557]},{\"text\":\"learning\",\"line\":64,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"online\",\"line\":64,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"learning\",\"line\":68,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektifitas\",\"line\":68,\"bahasa\":[\"swedish\",0.25303030303030305]},{\"text\":\"efesiensi\",\"line\":68,\"bahasa\":[\"turkish\",0.18259259259259253]},{\"text\":\"learning\",\"line\":74,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":76,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"login\",\"line\":78,\"bahasa\":[\"icelandic\",0.31199999999999994]},{\"text\":\"learning\",\"line\":82,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"literatur\",\"line\":82,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"Development\",\"line\":82,\"bahasa\":[\"french\",0.353030303030303]},{\"text\":\"Waterfall\",\"line\":82,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"Denzig\",\"line\":88,\"bahasa\":[\"danish\",0.4455555555555555]},{\"text\":\"tersetruktur\",\"line\":88,\"bahasa\":[\"latin\",0.30333333333333334]},{\"text\":\"Literatur\",\"line\":94,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"literatur\",\"line\":96,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"refrensi\",\"line\":96,\"bahasa\":[\"spanish\",0.21250000000000002]},{\"text\":\"learning\",\"line\":96,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"Development\",\"line\":100,\"bahasa\":[\"french\",0.353030303030303]},{\"text\":\"Waterfall\",\"line\":100,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"waterfall\",\"line\":100,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"Waterfall\",\"line\":104,\"bahasa\":[\"english\",0.4411111111111111]},{\"text\":\"Requiremenst\",\"line\":106,\"bahasa\":[\"french\",0.39861111111111114]},{\"text\":\"Analysis\",\"line\":106,\"bahasa\":[\"tagalog\",0.3154166666666667]},{\"text\":\"literatur\",\"line\":108,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"relavan\",\"line\":108,\"bahasa\":[\"dutch\",0.3271428571428572]},{\"text\":\"Design\",\"line\":110,\"bahasa\":[\"french\",0.3088888888888889]},{\"text\":\"Development\",\"line\":114,\"bahasa\":[\"french\",0.353030303030303]},{\"text\":\"Dibagian\",\"line\":116,\"bahasa\":[\"indonesian\",0.44666666666666666]},{\"text\":\"learning\",\"line\":120,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"tlah\",\"line\":120,\"bahasa\":[\"indonesian\",0.43666666666666665]},{\"text\":\"error\",\"line\":120,\"bahasa\":[\"norwegian\",0.374]},{\"text\":\"IPENDAHULUAN\",\"line\":130,\"bahasa\":[\"indonesian\",0.2758333333333334]},{\"text\":\"relavan\",\"line\":132,\"bahasa\":[\"dutch\",0.3271428571428572]},{\"text\":\"refrensinya\",\"line\":132,\"bahasa\":[\"indonesian\",0.2284848484848484]},{\"text\":\"IIIANALISIS\",\"line\":134,\"bahasa\":[\"tagalog\",0.22636363636363632]},{\"text\":\"case\",\"line\":136,\"bahasa\":[\"spanish\",0.47583333333333333]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"activity\",\"line\":136,\"bahasa\":[\"spanish\",0.2104166666666667]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"class\",\"line\":136,\"bahasa\":[\"estonian\",0.15466666666666673]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"entity\",\"line\":136,\"bahasa\":[\"latin\",0.3844444444444445]},{\"text\":\"relationship\",\"line\":136,\"bahasa\":[\"french\",0.38416666666666666]},{\"text\":\"diagram\",\"line\":136,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"output\",\"line\":136,\"bahasa\":[\"english\",0.2305555555555555]},{\"text\":\"input\",\"line\":136,\"bahasa\":[\"latin\",0.3946666666666667]},{\"text\":\"interface\",\"line\":136,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"VIMPLEMENTASI\",\"line\":138,\"bahasa\":[\"lithuanian\",0.3389743589743589]},{\"text\":\"output\",\"line\":138,\"bahasa\":[\"english\",0.2305555555555555]},{\"text\":\"input\",\"line\":138,\"bahasa\":[\"latin\",0.3946666666666667]},{\"text\":\"VIPENUTUP\",\"line\":140,\"bahasa\":[\"finnish\",0.14222222222222225]},{\"text\":\"learning\",\"line\":152,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"literatur\",\"line\":162,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"refrensi\",\"line\":162,\"bahasa\":[\"spanish\",0.21250000000000002]},{\"text\":\"learning\",\"line\":162,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"literatur\",\"line\":166,\"bahasa\":[\"latin\",0.5044444444444445]},{\"text\":\"relavan\",\"line\":166,\"bahasa\":[\"dutch\",0.3271428571428572]},{\"text\":\"case\",\"line\":170,\"bahasa\":[\"spanish\",0.47583333333333333]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"activity\",\"line\":170,\"bahasa\":[\"spanish\",0.2104166666666667]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"entity\",\"line\":170,\"bahasa\":[\"latin\",0.3844444444444445]},{\"text\":\"relationship\",\"line\":170,\"bahasa\":[\"french\",0.38416666666666666]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"class\",\"line\":170,\"bahasa\":[\"estonian\",0.15466666666666673]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"diagram\",\"line\":170,\"bahasa\":[\"indonesian\",0.31999999999999995]},{\"text\":\"tools\",\"line\":170,\"bahasa\":[\"english\",0.19733333333333325]},{\"text\":\"Draw\",\"line\":170,\"bahasa\":[\"slovene\",0.19833333333333336]},{\"text\":\"Interface\",\"line\":172,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"interdace\",\"line\":174,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"experience\",\"line\":174,\"bahasa\":[\"spanish\",0.2686666666666666]},{\"text\":\"tools\",\"line\":174,\"bahasa\":[\"english\",0.19733333333333325]},{\"text\":\"Adobe\",\"line\":174,\"bahasa\":[\"portuguese\",0.20266666666666666]},{\"text\":\"interface\",\"line\":174,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"interface\",\"line\":174,\"bahasa\":[\"romanian\",0.48777777777777775]},{\"text\":\"learning\",\"line\":174,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"framework\",\"line\":178,\"bahasa\":[\"pidgin\",0.18555555555555547]},{\"text\":\"laravel\",\"line\":178,\"bahasa\":[\"spanish\",0.42714285714285716]},{\"text\":\"HTML\",\"line\":178},{\"text\":\"Chrome\",\"line\":178,\"bahasa\":[\"italian\",0.2644444444444445]},{\"text\":\"local\",\"line\":178,\"bahasa\":[\"spanish\",0.3533333333333334]},{\"text\":\"XAMPP\",\"line\":178,\"bahasa\":[\"somali\",0.10733333333333328]},{\"text\":\"hosting\",\"line\":178,\"bahasa\":[\"english\",0.5033333333333334]},{\"text\":\"learning\",\"line\":182,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efektiftas\",\"line\":182,\"bahasa\":[\"swedish\",0.278]},{\"text\":\"efesiensi\",\"line\":182,\"bahasa\":[\"turkish\",0.18259259259259253]},{\"text\":\"online\",\"line\":182,\"bahasa\":[\"estonian\",0.4411111111111111]},{\"text\":\"Chrome\",\"line\":182,\"bahasa\":[\"italian\",0.2644444444444445]},{\"text\":\"Learning\",\"line\":184,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"learning\",\"line\":186,\"bahasa\":[\"norwegian\",0.3866666666666667]},{\"text\":\"efesien\",\"line\":186,\"bahasa\":[\"spanish\",0.28047619047619043]}]', '2021-07-15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan`
--

CREATE TABLE `kegiatan` (
  `IDKegiatan` int(11) NOT NULL,
  `IDUsers` bigint(20) NOT NULL,
  `Kegiatan` varchar(100) NOT NULL,
  `Tempat` varchar(100) NOT NULL,
  `JamKegiatan` time NOT NULL,
  `TanggalKegiatan` date NOT NULL,
  `Finish` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `konsentrasi`
--

CREATE TABLE `konsentrasi` (
  `IDKonsentrasi` int(11) NOT NULL,
  `IDJurusanKsn` int(11) NOT NULL,
  `IDDosen` bigint(20) NOT NULL,
  `Konsentrasi` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konsentrasi`
--

INSERT INTO `konsentrasi` (`IDKonsentrasi`, `IDJurusanKsn`, `IDDosen`, `Konsentrasi`) VALUES
(1, 1, 101, 'Software Engginering'),
(2, 2, 104, 'Perdata');

-- --------------------------------------------------------

--
-- Struktur dari tabel `notifikasi`
--

CREATE TABLE `notifikasi` (
  `IDNotifikasi` int(11) NOT NULL,
  `Notifikasi` varchar(300) NOT NULL,
  `Catatan` text NOT NULL,
  `TanggalNotifikasi` varchar(40) NOT NULL,
  `IDPenerima` bigint(20) NOT NULL,
  `IDPengirim` bigint(20) NOT NULL,
  `StatusNotifikasi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `notifikasi`
--

INSERT INTO `notifikasi` (`IDNotifikasi`, `Notifikasi`, `Catatan`, `TanggalNotifikasi`, `IDPenerima`, `IDPengirim`, `StatusNotifikasi`) VALUES
(7, 'SISTEM BIMBINGAN SKRIPSI ONLINE MENGGUNAKAN ALGORITMA KMP', 'Anda Di Tetapkan Sebagai Dosen Pembimbing Hamzah Padil Anda sekarang bisa mengacc proposal maupun skripsi Hamzah Padildan juga menambah kartu bimbingan untuk mahasiswa tersebut Anda ditetapkan sebagai pembimbing ke 1', '2021-07-15', 102, 101, 'Informasi'),
(8, 'SISTEM BIMBINGAN SKRIPSI ONLINE MENGGUNAKAN ALGORITMA KMP', 'Anda Di Tetapkan Sebagai Dosen Pembimbing Hamzah Padil Anda sekarang bisa mengacc proposal maupun skripsi Hamzah Padildan juga menambah kartu bimbingan untuk mahasiswa tersebut Anda ditetapkan sebagai pembimbing ke 2', '2021-07-15', 103, 101, 'Informasi'),
(9, 'SISTEM BIMBINGAN SKRIPSI ONLINE MENGGUNAKAN ALGORITMA KMP', 'acdsdfsdf', '2021-07-15', 1, 101, 'Diterima');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembimbing`
--

CREATE TABLE `pembimbing` (
  `IDPembimbing` int(11) NOT NULL,
  `IDDosenPmb` bigint(20) NOT NULL,
  `IDSkripsiPmb` int(11) NOT NULL,
  `StatusProposal` tinyint(1) NOT NULL,
  `StatusSkripsi` tinyint(1) NOT NULL,
  `StatusPembimbing` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembimbing`
--

INSERT INTO `pembimbing` (`IDPembimbing`, `IDDosenPmb`, `IDSkripsiPmb`, `StatusProposal`, `StatusSkripsi`, `StatusPembimbing`) VALUES
(5, 102, 1626323184, 0, 0, 1),
(6, 103, 1626323184, 0, 0, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `skripsi`
--

CREATE TABLE `skripsi` (
  `IDSkripsi` int(20) NOT NULL,
  `IDMahasiswaSkripsi` bigint(20) NOT NULL,
  `JudulSkripsi` varchar(200) NOT NULL,
  `QRCode` text NOT NULL,
  `FileProposal` varchar(100) NOT NULL,
  `FileSkripsi` varchar(100) NOT NULL,
  `Deskripsi` text NOT NULL,
  `Tanggal` date NOT NULL,
  `Nilai` int(100) NOT NULL,
  `File_Hash` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `skripsi`
--

INSERT INTO `skripsi` (`IDSkripsi`, `IDMahasiswaSkripsi`, `JudulSkripsi`, `QRCode`, `FileProposal`, `FileSkripsi`, `Deskripsi`, `Tanggal`, `Nilai`, `File_Hash`) VALUES
(1626323184, 1, 'SISTEM BIMBINGAN SKRIPSI ONLINE MENGGUNAKAN ALGORITMA KMP', '1.png', '1626323184.docx', '', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto a possimus voluptates? Tenetur temporibus repellendus ipsam dolorum at nobis fugiat id culpa! Dolores, repudiandae consectetur. Vero maiores quia quibusdam! Eveniet.', '2021-07-15', 0, 'e7338cbf79b39ba2b0a753dbbab6f7af');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `ID` bigint(20) NOT NULL,
  `Nama` varchar(30) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `IDJurusanUser` bigint(20) NOT NULL,
  `IDKonsentrasiUser` bigint(20) NOT NULL,
  `NoHP` varchar(20) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Foto` varchar(30) NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`ID`, `Nama`, `Password`, `IDJurusanUser`, `IDKonsentrasiUser`, `NoHP`, `Email`, `Foto`, `Status`) VALUES
(1, 'Hamzah Padil', '0b887f6ab6d69e6fcf753d8322279daf', 1, 1, '', 'hamzahf209@gmail.com', '', 'Skripsi'),
(2, 'Jojo', '7273036588484e992112034400aac406', 2, 2, '085156181571', 'hamzahf209@gmail.com', '2.png', 'Skripsi'),
(3, 'mahasiswa3', '', 1, 1, '', 'hamzahf209@gmail.com', '', 'Mahasiswa'),
(101, 'Kaprodi', '9f532e2e4642f3055551dcb77ee9e91d', 1, 1, '', 'hamzahf209@gmail.com', '', 'Dosen'),
(102, 'Pembimbing 1', '614578a1ac69d401b6fa6299180e25b8', 1, 1, '', 'hamzahf209@gmail.com', '', 'Dosen'),
(103, 'Pembimbing 2', '58fc412d2705f90778bc7deef2835487', 1, 1, '', 'hamzahf209@gmail.com', '', 'Dosen'),
(104, 'KapHuk', 'a943307c06df710b4a77951609c2b3ba', 2, 2, '', 'hamzahf209@gmail.com', '', 'Dosen'),
(105, 'PemHuk', '4e58c39f3b885de973851573de54f9fc', 2, 2, '', 'hamzahf209@gmail.com', '', 'Dosen'),
(106, 'PemHuk2', 'fefb147dbaa78cce2682712aef7de958', 2, 2, '', 'hamzahf209@gmail.com', '', 'Dosen');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ideskripsi`
--
ALTER TABLE `ideskripsi`
  ADD PRIMARY KEY (`IDIde`);

--
-- Indeks untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`IDJurusan`) USING BTREE;

--
-- Indeks untuk tabel `kartubimbingan`
--
ALTER TABLE `kartubimbingan`
  ADD PRIMARY KEY (`IDKartu`);

--
-- Indeks untuk tabel `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`IDKegiatan`),
  ADD KEY `IDUsers` (`IDUsers`);

--
-- Indeks untuk tabel `konsentrasi`
--
ALTER TABLE `konsentrasi`
  ADD PRIMARY KEY (`IDKonsentrasi`),
  ADD KEY `IDJurusanKsn` (`IDJurusanKsn`);

--
-- Indeks untuk tabel `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`IDNotifikasi`);

--
-- Indeks untuk tabel `pembimbing`
--
ALTER TABLE `pembimbing`
  ADD PRIMARY KEY (`IDPembimbing`);

--
-- Indeks untuk tabel `skripsi`
--
ALTER TABLE `skripsi`
  ADD PRIMARY KEY (`IDSkripsi`),
  ADD KEY `nim_mhs_skripsi` (`IDMahasiswaSkripsi`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `id_jurusan_mhs` (`IDJurusanUser`),
  ADD KEY `id_konsentrasi_mhs` (`IDKonsentrasiUser`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `IDJurusan` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kartubimbingan`
--
ALTER TABLE `kartubimbingan`
  MODIFY `IDKartu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `IDKegiatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `konsentrasi`
--
ALTER TABLE `konsentrasi`
  MODIFY `IDKonsentrasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `IDNotifikasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `pembimbing`
--
ALTER TABLE `pembimbing`
  MODIFY `IDPembimbing` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
